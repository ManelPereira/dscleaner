DS Cleaner
===========

DS Cleaner allows for easy cleanup of energy disagregation datasets and allows
for easy wave conversion.

This library is based in librosa and PySoundFile library.

Visit the Jupyter notebook for examples, available here: https://www.osf.io/vraqz

View the documentation in ReadTheDocs: https://dscleaner.readthedocs.io/en/latest/

View the paper "dsCleaner: A Python Library to Clean, Preprocess and Convert Non-Instrusive Load Monitoring Datasets", available in: https://doi.org/10.3390/data4030123


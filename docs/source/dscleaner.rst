DS Cleaner Package
==================

DSCleaner's class diagram
-------------------------
These are the main components of the package

.. figure:: dscleaner.png
    :alt: Dscleaner class diagram

dscleaner.IFileInfo class
--------------------------

.. automodule:: dscleaner.ifileinfo
    :members:
    :undoc-members:
    :show-inheritance:

dscleaner.CSVFileInfo class
----------------------------

.. automodule:: dscleaner.csvfileinfo
    :members:
    :undoc-members:
    :show-inheritance:

dscleaner.FileInfo class
-------------------------

.. automodule:: dscleaner.fileinfo
    :members:
    :undoc-members:
    :show-inheritance:

dscleaner.FileUtil class
-------------------------

.. automodule:: dscleaner.fileutil
    :members:
    :undoc-members:
    :show-inheritance:

dscleaner.FileWriter class
---------------------------

.. automodule:: dscleaner.filewriter
    :members:
    :undoc-members:
    :show-inheritance:

dscleaner.Merger class
-----------------------

.. automodule:: dscleaner.merger
    :members:
    :undoc-members:
    :show-inheritance:

dscleaner.Splitter class
----------------------
.. automodule:: dscleaner.splitter
    :members:
    :undoc-members:
    :show-inheritance:

dscleaner.Utils module
----------------------

.. automodule:: dscleaner.utils
    :members:
    :undoc-members:
    :show-inheritance:

Welcome to DSCleaner's documentation!
=====================================
.. include:: 
   intro.rst

.. toctree::
   :maxdepth: 2
   :caption: Package contents:

   dscleaner

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
